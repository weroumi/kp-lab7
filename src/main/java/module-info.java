module com.example.lab7 {
    requires javafx.controls;
    requires javafx.fxml;

    requires com.dlsc.formsfx;
    requires org.kordamp.bootstrapfx.core;

    opens com.example.lab7 to javafx.fxml;
    exports com.example.lab7;
    opens lab7 to javafx.fxml;
    exports lab7;
    opens lab7.controllers to javafx.fxml;
    exports lab7.controllers;
    opens lab7.models to javafx.fxml;
    exports lab7.models;

}