package lab7.models;

import java.util.concurrent.BlockingQueue;

public class ConsumerClass implements Runnable{
    private BlockingQueue<Product> productQueue = null;
    private Product consumedProduct = null;
    public ConsumerClass(Product consumedProduct, BlockingQueue<Product> queue){
        if(queue == null){
            throw new NullPointerException();
        }
        this.consumedProduct = consumedProduct;
        this.productQueue = queue;
    }
    @Override
    public void run(){
        while(true){
            if(this.productQueue.isEmpty()){
                System.out.println(Thread.currentThread().getName() + " nothing to consume");
            }
            try{
                Product product = this.productQueue.take();
                System.out.println(Thread.currentThread().getName() + " Consumed " + product.getName());
            } catch (InterruptedException e){
                e.printStackTrace();
            }
        }
    }

    @Override
    public String toString() {
        return consumedProduct.getName() + " consumer";
    }
}
