package lab7.models;

public class Product {
    private final String name;
    private final int resourcesToProduce;
    public Product(){
        this.name = "[default]";
        this.resourcesToProduce = 3;
    }
    public Product(String name, int resourcesToProduce){
        this.name = "[" + name + "]";
        this.resourcesToProduce = resourcesToProduce;
    }

    public int getResourcesToProduce() {
        return resourcesToProduce;
    }

    public String getName() {
        return name;
    }
}
