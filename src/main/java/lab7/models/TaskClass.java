package lab7.models;

import javafx.beans.property.SimpleStringProperty;

public class TaskClass{
    private SimpleStringProperty name;
    private SimpleStringProperty count;
    public TaskClass(String name, int count){
        this.count = new SimpleStringProperty(Integer.toString(count));
        this.name = new SimpleStringProperty(name);
    }

    public String getCount() {
        return count.get();
    }

    public String getName() {
        return name.get();
    }

    @Override
    public String toString() {
        return "TaskClass{" +
                "name='" + name.get() + '\'' +
                ", count=" + count.get() +
                '}';
    }
}
