package lab7.models;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class FactoryClass implements Runnable {
    private BlockingQueue<Product> productQueue = null;
    Product productToProduce;
    int resources;
    public FactoryClass(Product productToProduce, int resources){
        this.productToProduce = productToProduce;
        this.resources = resources;
        int capacity = resources / productToProduce.getResourcesToProduce();
        this.productQueue = new LinkedBlockingQueue<>(capacity);
    }

    public BlockingQueue<Product> getProductQueue() {
        return productQueue;
    }

    public void useResources(int numberOfResources){
        if(this.resources > 0){
            this.resources -= numberOfResources;
        }
    }
    public void produceGoods(){
        if(resources < productToProduce.getResourcesToProduce()){
            System.out.println("Not enough resources to produce product " + productToProduce);
        }
        try{
            useResources(productToProduce.getResourcesToProduce());
            this.productQueue.put(productToProduce);
            System.out.println(Thread.currentThread().getName() + " Produced " + productToProduce.getName());
        }catch (InterruptedException e){
            System.out.println("Factory was interrupted.");
        }
    }

    @Override
    public void run(){
        while(resources > productToProduce.getResourcesToProduce()){
            long timeMillis = System.currentTimeMillis();
            produceGoods();
            sleep();
        }
    }
    private void sleep(){
        try{
            Thread.sleep(1000);
        }catch (InterruptedException e){
            e.printStackTrace();
        }
    }

    public boolean isWorking() {
        return resources > productToProduce.getResourcesToProduce();
    }

    @Override
    public String toString() {
        return productToProduce.getName() + " factory";
    }
}
