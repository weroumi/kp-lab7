package lab7;

import javafx.collections.FXCollections;
import javafx.collections.ObservableArray;
import javafx.collections.ObservableList;
import lab7.models.ConsumerClass;
import lab7.models.FactoryClass;
import lab7.models.Product;
import lab7.models.TaskClass;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class App {
    private List<FactoryClass> factories;
    private List<ConsumerClass> consumers;
    ExecutorService executorService;

    public App(){
        this.factories =  new ArrayList<FactoryClass>();
        this.consumers =  new ArrayList<ConsumerClass>();
    }
    public void startThreads(){
        int totalThreadCount = factories.size() + consumers.size();
        executorService = Executors.newFixedThreadPool(totalThreadCount);
        List<Runnable> allTasks = Stream.concat(factories.stream(), consumers.stream()).toList();
        try {
            allTasks.forEach(task -> {
                executorService.submit(task);
            });
        } finally {
            executorService.shutdown();
        }
    }
    public void addFactoryAndConsumers(Product product, int resources, int countOfConsumers){
        FactoryClass newFactory = new FactoryClass(product, resources);
        factories.add(newFactory);
        for (int i = 0; i < countOfConsumers; ++i){
            consumers.add(new ConsumerClass(product, newFactory.getProductQueue()));
        }
    }

    public ObservableList<TaskClass>getTasks(){
        return FXCollections.observableArrayList(Stream.concat(
                factories.stream()
                        .collect(Collectors.groupingBy(f -> f.toString()))
                        .entrySet().stream()
                        .map(e -> new TaskClass(e.getKey(), e.getValue().size())),
                consumers.stream()
                        .collect(Collectors.groupingBy(c -> c.toString()))
                        .entrySet().stream()
                        .map(e -> new TaskClass(e.getKey(), e.getValue().size()))
        ).toList());
    }
}
