package lab7.controllers;

import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import lab7.App;
import lab7.models.Product;
import lab7.models.TaskClass;

import java.net.URL;
import java.util.ResourceBundle;

public class lab7Controller implements Initializable {
    @FXML
    private Spinner<Integer> productResourcesSpinner;
    @FXML
    private Spinner<Integer> productFactorySpinner;
    @FXML
    private Spinner<Integer> productConsumerSpinner;
    @FXML
    private Spinner<Integer> productFactoryResourcesSpinner;
    @FXML
    private TextField productNameTextField;
    @FXML
    private Button startButton;
    @FXML
    private Button addButton;
    @FXML
    private TableView taskTable;
    @FXML
    private TableView threadTable;

    @FXML
    TableColumn nameCol;

    @FXML
    TableColumn countCol;
    private ObservableList<TaskClass> data;
    private App app;
    @Override
    public void initialize(URL arg0, ResourceBundle arg1){
        SpinnerValueFactory<Integer> valueFactory = new SpinnerValueFactory.IntegerSpinnerValueFactory(1, 10);
        valueFactory.setValue(3);
        productResourcesSpinner.setValueFactory(valueFactory);

        valueFactory = new SpinnerValueFactory.IntegerSpinnerValueFactory(1, 1);
        valueFactory.setValue(1);
        productFactorySpinner.setValueFactory(valueFactory);

        valueFactory = new SpinnerValueFactory.IntegerSpinnerValueFactory(1, 16);
        valueFactory.setValue(1);
        productConsumerSpinner.setValueFactory(valueFactory);

        valueFactory = new SpinnerValueFactory.IntegerSpinnerValueFactory(20, 500);
        valueFactory.setValue(100);
        productFactoryResourcesSpinner.setValueFactory(valueFactory);

        app = new App();
        data = app.getTasks();
    }

    @FXML
    private void handleAddButtonClick(ActionEvent event){
        Product product = new Product(productNameTextField.getText(),
                                      productResourcesSpinner.getValue());
        app.addFactoryAndConsumers(product,
                                   productFactoryResourcesSpinner.getValue(),
                                   productConsumerSpinner.getValue());
        data = app.getTasks();

//        taskTable = new TableView();
//        nameCol = new TableColumn("name");
//        countCol = new TableColumn("count");
        nameCol.setCellValueFactory(
                new PropertyValueFactory<>("name"));
        countCol.setCellValueFactory(
                new PropertyValueFactory<>("count"));


        taskTable.setItems(data);
//        taskTable.getColumns().addAll(nameCol, countCol);

        app.getTasks().forEach(t -> {
            System.out.println(t.toString());
        });
    }
    @FXML
    private void handleStartButtonClick(ActionEvent event){
        app.startThreads();
    }
}
